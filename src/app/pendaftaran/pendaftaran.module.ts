import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
// import {PendaftaranComponent } from './pendaftaran.component';
import {PendaftaranRoutes} from './pendaftaran.routing';
import {IdentitasBaruComponent} from './identitas-baru/identitas-baru.component';
import {IdentitasLamaComponent} from './identitas-lama/identitas-lama.component';
import {PoliklinikComponent } from './poliklinik/poliklinik.component';
import {SharedModule} from '../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PendaftaranRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({})
  ],
  declarations: [
    // PendaftaranComponent,
    IdentitasLamaComponent,
    IdentitasBaruComponent,
    PoliklinikComponent
  ]
})
export class PendaftaranModule { }
