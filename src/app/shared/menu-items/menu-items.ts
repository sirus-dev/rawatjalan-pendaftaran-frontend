import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: 'Layout',
    main: [
      {
        state: 'dashboard',
        name: 'Dashboard',
        type: 'link',
        icon: 'icofont icofont-home'
      },
      {
        state: 'pendaftaran',
        name: 'Pendaftaran',
        type: 'link',
        icon: 'icofont icofont-contact-add'
      },
      {
        state: 'pasien',
        name: 'Pasien Poliklinik',
        type: 'link',
        icon: 'icofont icofont-contacts'
      },
      {
        state: 'error',
        name: 'Error',
        type: 'link',
        icon: 'icofont icofont-exclamation-tringle'
      },
      {
        state: 'login',
        name: 'Login',
        type: 'link',
        icon: 'icofont icofont-lock'
      },
      {
        state: 'forgot',
        name: 'Forgot',
        type: 'link',
        icon: 'icofont icofont-ui-password'
      },
    ]
  },
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
