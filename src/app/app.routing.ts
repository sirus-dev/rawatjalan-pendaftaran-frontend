import {Routes} from '@angular/router';

import {AdminLayoutComponent} from './layouts/admin/admin-layout.component';
import {AuthLayoutComponent} from './layouts/auth/auth-layout.component';

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '',
    component: AdminLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }, {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      }, {
        path: 'pendaftaran',
        loadChildren: './pendaftaran/pendaftaran.module#PendaftaranModule' 
      }, {
        path: 'pasien',
        loadChildren: './pasien/pasien.module#PasienModule'
      }
    ]
  }, 
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
      },
      {
        path: 'login',
        loadChildren: './authentication/login-email/login-email.module#LoginEmailModule'
      }, {
        path: 'login-password',
        loadChildren: './authentication/login-password/login-password.module#LoginPasswordModule'
      }, {
        path: 'forgot',
        loadChildren: './authentication/forgot/forgot.module#ForgotModule'
      }, {
        path: 'error',
        loadChildren: './error/error.module#ErrorModule'
      }
    ]
  }, 
  {
    path: '**',
    redirectTo: 'error/404'
  }
];
