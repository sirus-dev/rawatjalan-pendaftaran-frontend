import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from "@swimlane/ngx-datatable";
import {fadeInOutTranslate} from '../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from "ng2-toasty";
import swal from 'sweetalert2';


declare const $: any;
declare var Morris: any;

@Component({
  selector: 'app-pasien',
  templateUrl: './pasien.component.html',
  animations: [fadeInOutTranslate]
})

export class PasienComponent implements OnInit {
  @ViewChild('ngxLoading') ngxLoadingComponent: NgxLoadingComponent;
  @ViewChild('customLoadingTemplate') customLoadingTemplate: TemplateRef<any>; 
  public loading = false;
  showDialog:boolean = false;

  
  @ViewChild(DatatableComponent) table: DatatableComponent;
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
 
  position: string = 'bottom-right';
  title: string;
  msg: string;
  showClose: boolean = true;
  timeout: number = 5000;
  theme: string = 'bootstrap';
  type: string = 'default';
  closeOther: boolean = false;

  columns = [
    { prop: 'id' },
    { name: 'nama' },
    { name: 'tgl_lahir' },
    { name: 'umur' },
    { name: 'tgl_daftar' },
    { name: 'status_daftar' },
    { name: 'action' }
  ];

  rowsFilter = [];
  tempFilter = [];
  

  constructor(
    private toastyService: ToastyService) { 

    this.fetchFilterData((data) => {
      this.tempFilter = [...data];
      this.rowsFilter = data;
    });
  }

  ngOnInit() {
  }


  fetchFilterData(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/pasien.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    const temp = this.tempFilter.filter(function(d) {
      return d.nama.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.rowsFilter = temp;
    this.table.offset = 0;
  }

  updateStatus(event) {
    const val = event.target.value.toLowerCase();

    console.log(val);
    const temp = this.tempFilter.filter(function(d) {
      return d.status_daftar.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.rowsFilter = temp;
    this.table.offset = 0;
  }

  
  showDetail() {
    this.loading = true;
    setTimeout(function() {
      this.loading = false;
      document.getElementById('btnShowDetail').click();
    }.bind(this), 2000);
  }
  
  showPrint() {
    this.loading = true;
    setTimeout(function() {
      this.loading = false;
      document.getElementById('btnShowPrint').click();
    }.bind(this), 2000);
  }

  addToast(options) {
    if(options.closeOther) {
      this.toastyService.clearAll();
    }
    this.position = options.position ? options.position : this.position;
    let toastOptions: ToastOptions = {
      title: options.title,
      msg: options.msg,
      showClose: options.showClose,
      timeout: options.timeout,
      theme: options.theme,
      onAdd: (toast: ToastData) => {
        console.log('Toast ' + toast.id + ' has been added!');
      },
      onRemove: (toast: ToastData) => {
        console.log('Toast ' + toast.id + ' has been added removed!');
      }
    };

    switch (options.type) {
      case 'default': this.toastyService.default(toastOptions); break;
      case 'info': this.toastyService.info(toastOptions); break;
      case 'success': this.toastyService.success(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
    }
  }

  showConfirmDelete() {
    swal({
      title: 'Konfirmasi Batal',
      text: "Anda yakin membatalkan pendaftaran pasien ini?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success m-r-10',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(function () {
      swal(
          'Info!',
          'Pendaftaran pasien berhasil dibatalkan.',
          'success'
      )
    }, function (dismiss) {}
    ).catch(swal.noop);
  }

}
