import {Routes} from '@angular/router';
import {LoginPasswordComponent} from './login-password.component';

export const LoginPasswordRoutes: Routes = [
    {
        path: '',
        component: LoginPasswordComponent,
        data: {
            breadcrumb: 'Login'
        }
    }
];
