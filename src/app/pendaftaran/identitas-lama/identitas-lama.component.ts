import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import { Router, NavigationEnd } from '@angular/router';
import { NgbDateCustomParserFormatter } from '../../shared/helpers/dateformat';
import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

declare const $: any;
declare var Morris: any;

@Component({
  selector: 'app-identitas-lama',
  templateUrl: './identitas-lama.component.html',
  styleUrls: ['./identitas-lama.component.css'],
  providers: [
    {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}
   ]
})

export class IdentitasLamaComponent implements OnInit {
  @ViewChild('ngxLoading') ngxLoadingComponent: NgxLoadingComponent;
  @ViewChild('customLoadingTemplate') customLoadingTemplate: TemplateRef<any>; 
  public loading = false;

  public kategoryValue: string;
  public kategoryName: string;
  public agamaValue: string;
  public agamaName: string;
  public agamaStatus: number;
  public pendidikanValue: string;
  public pendidikanName: string;
  public pendidikanStatus: number;
  public pekerjaanValue: string;
  public pekerjaanName: string;
  public pekerjaanStatus: number;
  public tinggalBersamaValue: string;
  public tinggalBersamaName: string;
  public tinggalBersamaStatus: number;
  public formIdentitasStatus: number;

  constructor(private router: Router) {
    this.formIdentitasStatus = 0;
  }

  showDataPasien() {
    this.loading = true;
    setTimeout(function() {
      this.loading = false;
      document.getElementById('btnShowPasien').click();
    }.bind(this), 2000);
  }

  showFormIdentitas() {
    if (this.formIdentitasStatus > 0) {
      this.formIdentitasStatus = 0;
    }else {
      this.formIdentitasStatus = 1;
    }
  }

  showFormRujukan() {
    this.loading = true;
    setTimeout(function() {
      this.loading = false;
      document.getElementById('btnShowRujukan').click();
    }.bind(this), 2000);
  }

  selectKategori(args) {
    this.kategoryValue = args.target.value;
    this.kategoryName = args.target.options[args.target.selectedIndex].text;
  }

  selectAgama(args) {
    this.agamaValue = args.target.value;
    this.agamaName = args.target.options[args.target.selectedIndex].text;
    if (this.agamaValue === 'Lainnya') {
      this.agamaStatus = 0;
      this.agamaName = '';
    }else {
      this.agamaStatus = 1;
    }
  }

  
  selectPendidikan(args) {
    this.pendidikanValue = args.target.value;
    this.pendidikanName = args.target.options[args.target.selectedIndex].text;
    if (this.pendidikanValue === 'Lainnya') {
      this.pendidikanStatus = 0;
      this.pendidikanName = '';
    }else {
      this.pendidikanStatus = 1;
    }
  }
  
  selectPekerjaan(args) {
    this.pekerjaanValue = args.target.value;
    this.pekerjaanName = args.target.options[args.target.selectedIndex].text;
    if (this.pekerjaanValue === 'Lainnya') {
      this.pekerjaanStatus = 0;
      this.pekerjaanName = '';
    }else {
      this.pekerjaanStatus = 1;
    }
  }


  selectTinggalBersama(args) {
    this.tinggalBersamaValue = args.target.value;
    this.tinggalBersamaName = args.target.options[args.target.selectedIndex].text;
    if (this.tinggalBersamaValue === 'Lainnya') {
      this.tinggalBersamaStatus = 0;
      this.tinggalBersamaName = '';
    }else {
      this.tinggalBersamaStatus = 1;
    }
  }


  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      window.scrollTo(0, 0)
    });
  }
}
