import {NgModule} from '@angular/core';
import {ForgotComponent} from './forgot.component';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {ForgotRoutes} from './forgot.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ForgotRoutes),
        SharedModule
    ],
    declarations: [ForgotComponent]
})

export class ForgotModule {}
