import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pendaftaran',
  template: '<router-outlet><spinner></spinner></router-outlet>'
})
export class PendaftaranComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
