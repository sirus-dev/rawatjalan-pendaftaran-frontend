import {NgModule} from '@angular/core';
import {LoginEmailComponent} from './login-email.component';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {LoginEmailRoutes} from './login-email.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(LoginEmailRoutes),
        SharedModule
    ],
    declarations: [LoginEmailComponent]
})

export class LoginEmailModule {}
