import { Routes } from '@angular/router';
import {IdentitasBaruComponent} from './identitas-baru/identitas-baru.component';
import {IdentitasLamaComponent} from './identitas-lama/identitas-lama.component';
import {PoliklinikComponent} from './poliklinik/poliklinik.component';

export const PendaftaranRoutes: Routes = [
    {
        path: '',
        data: {
            breadcrumb: 'Pendaftaran',
            status: false
        },
        children: [
            {
                path: '',
                redirectTo: 'identitas-baru',
                pathMatch: 'full'
            }, {
                path: 'identitas-baru',
                component: IdentitasBaruComponent,
                data: {
                    breadcrumb: 'Identitas Pasien Baru',
                    status: true
                } 
            }, {
                path: 'identitas-lama',
                component: IdentitasLamaComponent,
                data: {
                    breadcrumb: 'Identitas Pasien Lama',
                    status: true
                }
            }, {
                path: 'poliklinik',
                component: PoliklinikComponent,
                data: {
                    breadcrumb: 'Poliklinik',
                    status: true
                }
            }
        ]
    }
];
